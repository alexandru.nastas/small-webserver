#!/bin/bash

### $1 - instance-name
### $2 - ip address (x.x.x.x)
### $3 - netmask (y.y.y.y)
### $4 - gateway (z.z.z.z)

if [ $# -eq 0 ]
  then
    echo "No argument supplied"
    echo "Usage: ./launcher.sh <webserver hostname> <ip address> <subnet mask> <gateway>"
    echo "Example : ./launcher.sh myweb1 10.0.0.20 255.255.255.0 10.0.0.1"
    exit 1
fi

$METADATAFILE=meta-data
$USERDATAFILE=user-data
$QCOWIMAGE=alpine.qcow2
$CIFILENAME=$1-ci.iso

echo "Building meta-data and user-data files"
echo "======================================="

echo "Building meta-data file"
cat > $METADATAFILE << EOF
instance-id: $1
local-hostname: $1
EOF

echo "Building user-data file"
cat > $USERDATAFILE << EOF
#cloud-config

write_files:
  - path: /etc/network/interfaces
    content: |
      auto lo
      iface lo inet loopback

      auto eth0
      iface eth0 inet static
              address $2
              netmask $3
              gateway $4

  - path: /www/index.html
    content: |
      <!DOCTYPE html>
      <html lang="en">
      <head>
         <meta charset="utf-8" />
         <title>$1 webserver</title>
      </head>
      <body>
         My hostname: $1 <br>
         My IP: $2
      </body>
      </html>
runcmd:
  - /etc/init.d/networking restart
EOF

echo "Creating cloud-init ISO"
genisoimage -output $CIFILENAME -volid cidata -joliet -rock $USERDATAFILE $METADATAFILE 2>build.log
cp -f $CIFILENAME /var/lib/libvirt/images

echo "Cleaning UP"
rm -rf $METADATAFILE $USERDATAFILE $CIFILENAME

echo "Copying and configuring Alpine-image qcow2 (200mb)"
cp $QCOWIMAGE /var/lib/libvirt/images/$1.qcow2
cat > /etc/libvirt/qemu/$1.xml << EOF

<domain type='kvm'>
  <name>$1</name>
  <memory unit='KiB'>262144</memory>
  <currentMemory unit='KiB'>262144</currentMemory>
  <vcpu placement='static'>1</vcpu>
  <os>
    <type arch='x86_64' machine='pc-i440fx-rhel7.0.0'>hvm</type>
    <boot dev='hd'/>
  </os>
  <features>
    <acpi/>
    <apic/>
    <pae/>
  </features>
  <clock offset='utc'>
    <timer name='rtc' tickpolicy='catchup'/>
    <timer name='pit' tickpolicy='delay'/>
    <timer name='hpet' present='no'/>
  </clock>
  <on_poweroff>destroy</on_poweroff>
  <on_reboot>restart</on_reboot>
  <on_crash>restart</on_crash>
  <devices>
    <emulator>/usr/libexec/qemu-kvm</emulator>
    <disk type='file' device='disk'>
      <driver name='qemu' type='qcow2'/>
      <source file='/var/lib/libvirt/images/$1.qcow2'/>
      <target dev='hda' bus='ide'/>
      <address type='drive' controller='0' bus='0' target='0' unit='0'/>
    </disk>
    <disk type='file' device='cdrom'>
      <driver name='qemu' type='raw'/>
      <source file='/var/lib/libvirt/images/$CIFILENAME'/>
      <target dev='hdc' bus='ide'/>
      <readonly/>
      <address type='drive' controller='0' bus='1' target='0' unit='0'/>
    </disk>
    <controller type='usb' index='0' model='ich9-ehci1'>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x06' function='0x7'/>
    </controller>
    <controller type='usb' index='0' model='ich9-uhci1'>
      <master startport='0'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x06' function='0x0' multifunction='on'/>
    </controller>
    <controller type='usb' index='0' model='ich9-uhci2'>
      <master startport='2'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x06' function='0x1'/>
    </controller>
    <controller type='usb' index='0' model='ich9-uhci3'>
      <master startport='4'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x06' function='0x2'/>
    </controller>
    <controller type='pci' index='0' model='pci-root'/>
    <controller type='ide' index='0'>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x01' function='0x1'/>
    </controller>
    <controller type='virtio-serial' index='0'>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x05' function='0x0'/>
    </controller>
    <interface type='bridge'>
      <source bridge='ovs'/>
      <virtualport type='openvswitch'>
      </virtualport>
      <target dev='$1'/>
      <model type='virtio'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
    </interface>
    <serial type='pty'>
      <target port='0'/>
    </serial>
    <console type='pty'>
      <target type='serial' port='0'/>
    </console>
    <channel type='spicevmc'>
      <target type='virtio' name='com.redhat.spice.0'/>
      <address type='virtio-serial' controller='0' bus='0' port='1'/>
    </channel>
    <input type='mouse' bus='ps2'/>
    <input type='keyboard' bus='ps2'/>
    <graphics type='spice' autoport='yes'/>
    <graphics type='vnc' port='-1' autoport='yes' listen='0.0.0.0'>
      <listen type='address' address='0.0.0.0'/>
    </graphics>
    <sound model='ich6'>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x04' function='0x0'/>
    </sound>
    <video>
      <model type='qxl' ram='65536' vram='65536' vgamem='16384' heads='1'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>
    </video>
    <redirdev bus='usb' type='spicevmc'>
    </redirdev>
    <redirdev bus='usb' type='spicevmc'>
    </redirdev>
    <memballoon model='virtio'>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x07' function='0x0'/>
    </memballoon>
  </devices>
</domain>

EOF

echo "Starting the instance"
echo "Issue virsh console $1 to connect"
virsh define /etc/libvirt/qemu/$1.xml
virsh start $1
